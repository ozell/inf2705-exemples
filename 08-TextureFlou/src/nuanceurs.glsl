// "#version 4xx\n#define NUANCEUR_...\n" doit être ajouté en préambule au chargement de chaque nuanceur
////////////////////////////////////////////////////////////////////////////////
#if defined(NUANCEUR_SOMMETS)

uniform mat4 matrModel, matrVisu, matrProj;

in vec4 Vertex;
in vec2 TexCoord;
out vec2 texCoord;

void main( void )
{
    // appliquer la transformation standard du sommet
    gl_Position = matrProj * matrVisu * matrModel * Vertex;

    // transmettre au nuanceur de fragments les coordonnées de texture reçues
    texCoord = TexCoord;
}

////////////////////////////////////////////////////////////////////////////////
#elif defined(NUANCEUR_FRAGMENTS)

uniform sampler2D laTextureScene;

uniform ivec2 poscour; // la position courante de la souris
uniform ivec2 taillecour; // la taille courante de la fenêtre
uniform int typeFlou; // le type de flou

// in vec4 gl_FragCoord;
in vec2 texCoord;
out vec4 FragColor;

vec4 flouter( )
{
    float offset[3] = float[]( 0.0, 1.3846153846, 3.2307692308 );
    float weight[3] = float[]( 0.2270270270, 0.3162162162, 0.0702702703 );

    vec3 coul = texture( laTextureScene, texCoord ).rgb * weight[0];
    for ( int i=1 ; i<3 ; i++ )
    {
        coul += texture( laTextureScene, texCoord + vec2(offset[i])/length(taillecour) ).rgb * weight[i];
        coul += texture( laTextureScene, texCoord - vec2(offset[i])/length(taillecour) ).rgb * weight[i];
    }
    return vec4( coul, 1.0 );
}

// vec4 floutertropsimple( )
// {
//    return ( texture( laTextureScene, texCoord ) +
//             textureOffset( laTextureScene, texCoord, 1*ivec2(0,-1) ) +
//             textureOffset( laTextureScene, texCoord, 1*ivec2(0,1) ) +
//             textureOffset( laTextureScene, texCoord, 1*ivec2(1,0) ) +
//             textureOffset( laTextureScene, texCoord, 1*ivec2(-1,0) ) +
//             textureOffset( laTextureScene, texCoord, 2*ivec2(0,-1) ) +
//             textureOffset( laTextureScene, texCoord, 2*ivec2(0,1) ) +
//             textureOffset( laTextureScene, texCoord, 2*ivec2(1,0) ) +
//             textureOffset( laTextureScene, texCoord, 2*ivec2(-1,0) ) +
//             textureOffset( laTextureScene, texCoord, 3*ivec2(0,-1) ) +
//             textureOffset( laTextureScene, texCoord, 3*ivec2(0,1) ) +
//             textureOffset( laTextureScene, texCoord, 3*ivec2(1,0) ) +
//             textureOffset( laTextureScene, texCoord, 3*ivec2(-1,0) )
//             )/13.;
// }

void main( void )
{
    switch ( typeFlou )
    {
    case 0:
        // flouter à gauche d'une ligne verticale
        if ( gl_FragCoord.x > poscour.x )
            FragColor = texture( laTextureScene, texCoord );
        else if ( gl_FragCoord.x > poscour.x-2 )
            FragColor = vec4( 1.0 );
        else
        {
            FragColor = flouter( );
            //FragColor = mix( flouter( ), vec4(1.0), 0.1 );
        }
        break;
    case 1:
        // flouter à droite d'une ligne verticale
        if ( gl_FragCoord.x > poscour.x )
        {
            FragColor = flouter( );
            //FragColor = mix( flouter( ), vec4(1.0), 0.1 );
        }
        else if ( gl_FragCoord.x > poscour.x-2 )
            FragColor = vec4( 1.0 );
        else
            FragColor = texture( laTextureScene, texCoord );
        break;
    case 2:
        {
        // flouter hors du cercle
        const float rayon = 80;
        float dist = distance( gl_FragCoord.xy, poscour );
        if ( dist < rayon )
            FragColor = texture( laTextureScene, texCoord );
        else if ( dist < rayon+2 )
            FragColor = vec4( 1.0 );
        else
        {
            FragColor = flouter( );
            //FragColor = mix( flouter( ), vec4(1.0), 0.1 );
        }
        }
        break;
    case 3:
        {
        // flouter dans le cercle
        const float rayon = 80;
        float dist = distance( gl_FragCoord.xy, poscour );
        if ( dist < rayon )
        {
            FragColor = flouter( );
            //FragColor = mix( flouter( ), vec4(1.0), 0.1 );
        }
        else if ( dist < rayon+2 )
            FragColor = vec4( 1.0 );
        else
            FragColor = texture( laTextureScene, texCoord );
        }
        break;
    }
}

#endif
