// "#version 4xx\n#define NUANCEUR_...\n" doit être ajouté en préambule au chargement de chaque nuanceur
////////////////////////////////////////////////////////////////////////////////
#if defined(NUANCEUR_SOMMETS)

uniform mat4 matrModel, matrVisu, matrProj;

in vec4 Vertex;

void main( void )
{
    // appliquer la transformation standard du sommet
    gl_Position = matrProj * matrVisu * matrModel * Vertex;
}

////////////////////////////////////////////////////////////////////////////////
#elif defined(NUANCEUR_FRAGMENTS)

uniform ivec2  taillecour; // taille de la fenetre en pixels
uniform float  temps;

vec2 hash12( float p )
{
    return fract( vec2( sin(p * 591.32), cos(p * 391.32) ) );
}

float hash21( in vec2 n )
{
    return fract(sin(dot( n, vec2(12.9898, 4.1414)) ) * 43758.5453);
}

vec2 hash22( in vec2 p )
{
    p = vec2( dot(p,vec2(127.1,311.7)), dot(p,vec2(269.5,183.3)) );
    return fract(sin(p)*43758.5453);
}

mat2 makerotation( in float theta )
{
    float c = cos(theta);
    float s = sin(theta);
    return mat2( c, -s, s, c );
}

float field1( in vec2 p )
{
    vec2 n = floor(p)-0.5;
    vec2 f = fract(p)-0.5;
    vec2 o = hash22(n)*.35;
    vec2 r = - f - o;
    r *= makerotation( 3.*temps + hash21(n)*3 );

    float epaisseur = 0.0;
    float longueur = 0.13;

    float d = 1.0-smoothstep( epaisseur, epaisseur+0.09, abs(r.x) );
    d *= 1. - smoothstep( longueur, longueur+0.02, abs(r.y) );

    float d2 = 1.0-smoothstep( epaisseur, epaisseur+0.09, abs(r.y) );
    d2 *= 1. - smoothstep( longueur, longueur+0.02, abs(r.x) );

    return max( d, d2 );
}

out vec4 FragColor;

void main( void )
{
    float scale = 90.;
    int layers  = 15;

    vec2 p = gl_FragCoord.xy / taillecour.xy - 0.5;
    p.x *= taillecour.x / taillecour.y;

    float mul = ( taillecour.x + taillecour.y ) / scale;

    vec3 col = vec3(0.);
    for ( int i = 0 ; i < layers ; i++ )
    {
        vec2 ds = hash12( i*2.5 )*.20;
        col = max( col, field1( (p+ds)*mul ) * (sin(ds.x*5100. + vec3(1.,2.,3.5))*.4 + .6) );
    }

    FragColor = vec4( col, 1.0 );
}

#endif
