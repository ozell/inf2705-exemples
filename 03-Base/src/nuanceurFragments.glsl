#version 410

// in vec4 gl_FragCoord;   // <-- déclaration implicite
// in bool gl_FrontFacing; // <-- déclaration implicite
// in vec2 gl_PointCoord;  // <-- déclaration implicite

in Attribs {
    vec4 couleurAvant;
    vec4 couleurArriere;
    vec4 pos; // position dans le repère de la caméra
} AttribsIn;

// out float gl_FragDepth; // <-- déclaration implicite
out vec4 FragColor;

uniform int solution;

void main(void)
{
    // assigner la couleur du fragment qui est la couleur interpolée
    vec4 coul = gl_FrontFacing ? AttribsIn.couleurAvant : AttribsIn.couleurArriere;

    // coul = vec4( 1.0, 0.5, 0.5, 1.0 );

    // À essayer :
    // if ( 460.0 < gl_FragCoord.x && gl_FragCoord.x < 540.0 ) discard;
    // if ( 400.0 < gl_FragCoord.x && gl_FragCoord.x < 600.0 ) { coul.rgb = vec3(1.); }
    // if ( gl_FragCoord.y > 300.0 ) discard;

    // if ( coul.r < .4 && coul.g < .4 && coul.b < .4 ) discard;
    // if ( coul.r > .5 && coul.g > .5 && coul.b > .5 ) discard;
    // if ( coul.r < .4 && coul.g < .4 ) discard;

    // if ( abs(AttribsIn.pos.x) > 1.7 ) discard; // en coordonnées de visualisation

    // assigner la couleur finale du fragment
    FragColor = coul;

    float x = gl_FragCoord.x - 400.;
    float y = gl_FragCoord.y - 300.;
    switch (solution)
    {
    default:
    case 1:
        // FragColor = coul; // (déjà fait ci-dessus)
        break;
    case 2:
        if ( sqrt( x*x + y*y ) < 180. ) { coul.rgb = vec3(1.); }
        break;
    case 3:
        if ( sqrt( x*x + y*y ) < 150. ) discard;
        break;
    case 4:
        if ( coul.r * coul.g * coul.b > .25 ) discard;
        break;
    case 5:
        break;
    case 6:
        break;
    }
}
