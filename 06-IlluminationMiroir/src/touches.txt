Touches possibles :
    q:  Quitter l'application
    x:  Activer/désactiver l'affichage des axes
    v:  Recharger les nuanceurs
    g:  Permuter l'affichage en fil de fer ou plein
    ESPACE:  Mettre en pause ou reprendre l'animation
    CROCHETGAUCHE:  Inverser la rotation
    CROCHETDROIT:  Remettre la rotation normalement
    0:  Remise de l'angle de la caméra à zéro
    DROITE:  Incrémenter l'angle de la caméra
    GAUCHE:  Décrementer l'angle de la caméra
    m:  Afficher ou non les murs
    t:  Afficher ou non le theiere
    c:  Utiliser ou non un plan de coupe
    l:  Limiter ou non la rotation de la caméra
    n:  Utiliser ou non les normales calculées comme couleur (pour le débogage)
    s:  Sauvegarder une copie de la fenêtre dans un fichier
