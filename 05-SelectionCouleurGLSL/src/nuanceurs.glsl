// "#version 4xx\n#define NUANCEUR_...\n" doit être ajouté en préambule au chargement de chaque nuanceur
////////////////////////////////////////////////////////////////////////////////
#if defined(NUANCEUR_SOMMETS)

uniform mat4 matrModel;
uniform mat4 matrVisu;
uniform mat4 matrProj;
uniform int numObjet;

layout(location=0) in vec4 Vertex;
layout(location=3) in vec4 Color;

// out gl_PerVertex // <-- déclaration implicite
// {
//   vec4 gl_Position;
//   float gl_PointSize;
//   float gl_ClipDistance[];
// };

out Attribs {
    vec4 couleur;
} AttribsOut;

void main(void)
{
    // appliquer la transformation standard du sommet (P * V * M * sommet)
    gl_Position = matrProj * matrVisu * matrModel * Vertex;

    // assigner la couleur du sommet
    if ( numObjet == 0 )
        AttribsOut.couleur = Color;
    else
        AttribsOut.couleur = vec4( numObjet*10.0/255., 0.0, 0.0, 1.0 );
}

////////////////////////////////////////////////////////////////////////////////
#elif defined(NUANCEUR_FRAGMENTS)

in Attribs {
    vec4 couleur;
} AttribsIn;

out vec4 FragColor;

void main(void)
{
    // assigner la couleur du fragment qui est la couleur interpolée
    FragColor = AttribsIn.couleur;
}

#endif
