// "#version 4xx\n#define NUANCEUR_...\n" doit être ajouté en préambule au chargement de chaque nuanceur
////////////////////////////////////////////////////////////////////////////////
#if defined(NUANCEUR_SOMMETS)

// ==> Les variables en commentaires ci-dessous sont déclarées implicitement:
// out gl_PerVertex
// {
//   vec4 gl_Position;
//   float gl_PointSize;
//   float gl_ClipDistance[];
// };

layout(location=0) in vec4 Vertex;
layout(location=3) in vec4 Color;

uniform mat4 matrModel;

out Attribs {
    vec4 couleur;
} AttribsOut;

void main(void)
{
    gl_Position = matrModel * Vertex;
    AttribsOut.couleur = Color;
}

////////////////////////////////////////////////////////////////////////////////
#elif defined(NUANCEUR_TESSCTRL)

// ==> Les variables en commentaires ci-dessous sont déclarées implicitement:
// in int gl_PatchVerticesIn;
// in int gl_PrimitiveID;
// in int gl_InvocationID;
// in gl_PerVertex
// {
//   vec4 gl_Position;
//   float gl_PointSize;
//   float gl_ClipDistance[];
// } gl_in[gl_MaxPatchVertices];

// patch out float gl_TessLevelOuter[4];
// patch out float gl_TessLevelInner[2];
// out gl_PerVertex
// {
//   vec4 gl_Position;
//   float gl_PointSize;
//   float gl_ClipDistance[];
// } gl_out[];

layout(vertices = 6) out;

uniform float TessLevelInner;
uniform float TessLevelOuter;

in Attribs {
    vec4 couleur;
} AttribsIn[];

out Attribs {
    vec4 couleur;
} AttribsOut[];

void main()
{
    gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;

    if ( gl_InvocationID == 0 )
    {
        gl_TessLevelInner[0] = TessLevelInner;
        gl_TessLevelOuter[0] = TessLevelOuter;
        gl_TessLevelOuter[1] = TessLevelOuter;
        gl_TessLevelOuter[2] = TessLevelOuter;
    }

    AttribsOut[gl_InvocationID].couleur = AttribsIn[gl_InvocationID].couleur;
}

////////////////////////////////////////////////////////////////////////////////
#elif defined(NUANCEUR_TESSEVAL)

// ==> Les variables en commentaires ci-dessous sont déclarées implicitement:
// in vec3 gl_TessCoord;
// in int gl_PatchVerticesIn;
// in int gl_PrimitiveID;
// patch in float gl_TessLevelOuter[4];
// patch in float gl_TessLevelInner[2];
// in gl_PerVertex
// {
//   vec4 gl_Position;
//   float gl_PointSize;
//   float gl_ClipDistance[];
// } gl_in[gl_MaxPatchVertices];

// out gl_PerVertex
// {
//   vec4 gl_Position;
//   float gl_PointSize;
//   float gl_ClipDistance[];
// };

layout(isolines) in;

in Attribs {
    vec4 couleur;
} AttribsIn[];

out Attribs {
    vec4 couleur;
} AttribsOut;

void main()
{
    float t = gl_TessCoord.x;
    // absolument pas efficace d'utiliser pow(), mais ça permet de bien voir les coefficients et les Nij(t)
    gl_Position = (  1. * pow((1.-t),5) * pow(t,0) * gl_in[0].gl_Position +
                     5. * pow((1.-t),4) * pow(t,1) * gl_in[1].gl_Position +
                     10. * pow((1.-t),3) * pow(t,2) * gl_in[2].gl_Position +
                     10. * pow((1.-t),2) * pow(t,3) * gl_in[3].gl_Position +
                     5. * pow((1.-t),1) * pow(t,4) * gl_in[4].gl_Position +
                     1. * pow((1.-t),0) * pow(t,5) * gl_in[5].gl_Position );
    AttribsOut.couleur = AttribsIn[0].couleur;
}

////////////////////////////////////////////////////////////////////////////////
#elif defined(NUANCEUR_GEOMETRIE)

// ==> Les variables en commentaires ci-dessous sont déclarées implicitement:
// in gl_PerVertex
// {
//   vec4 gl_Position;
//   float gl_PointSize;
//   float gl_ClipDistance[];
// } gl_in[];

// out gl_PerVertex
// {
//   vec4 gl_Position;
//   float gl_PointSize;
//   float gl_ClipDistance[];
// };

layout(lines) in;
layout(line_strip, max_vertices = 4) out;

uniform mat4 matrVisu;
uniform mat4 matrProj;

in Attribs {
    vec4 couleur;
} AttribsIn[];

out Attribs {
    vec4 couleur;
} AttribsOut;

void main()
{
    // émettre les sommets
    for ( int i = 0 ; i < gl_in.length() ; ++i )
    {
        gl_Position = matrProj * matrVisu * gl_in[i].gl_Position;
        AttribsOut.couleur = AttribsIn[i].couleur;
        EmitVertex();
    }
}

////////////////////////////////////////////////////////////////////////////////
#elif defined(NUANCEUR_FRAGMENTS)

// ==> Les variables en commentaires ci-dessous sont déclarées implicitement:
// in vec4 gl_FragCoord;
// in bool gl_FrontFacing;
// in vec2 gl_PointCoord;
// out float gl_FragDepth;

in Attribs {
    vec4 couleur;
} AttribsIn;

// out float gl_FragDepth;
out vec4 FragColor;

void main(void)
{
    FragColor = AttribsIn.couleur;
}

#endif
