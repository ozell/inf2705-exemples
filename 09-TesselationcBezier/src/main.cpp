#include <stdlib.h>
#include <iostream>
#include <math.h>
#include "inf2705-matrice.h"
#include "inf2705-nuanceur.h"
#include "inf2705-fenetre.h"
#include "Etat.h"
#include "Pipeline.h"

void chargerNuanceurs()
{
    {
        // créer le programme
        prog1 = glCreateProgram();

        // attacher le nuanceur de sommets
        const GLchar *chainesSommets[2] = { "#version 410\n#define NUANCEUR_SOMMETS\n", ProgNuanceur::lireNuanceur( "nuanceurs.glsl" ) };
        if ( chainesSommets[1] != NULL )
        {
            GLuint nuanceurObj = glCreateShader( GL_VERTEX_SHADER );
            glShaderSource( nuanceurObj, 2, chainesSommets, NULL );
            glCompileShader( nuanceurObj );
            glAttachShader( prog1, nuanceurObj );
            ProgNuanceur::afficherLogCompile( nuanceurObj );
            delete [] chainesSommets[1];
        }
        // attacher le nuanceur de controle de la tessellation
        const GLchar *chainesTessctrl[2] = { "#version 410\n#define NUANCEUR_TESSCTRL\n", ProgNuanceur::lireNuanceur( "nuanceurs.glsl" ) };
        if ( chainesTessctrl[1] != NULL )
        {
            GLuint nuanceurObj = glCreateShader( GL_TESS_CONTROL_SHADER );
            glShaderSource( nuanceurObj, 2, chainesTessctrl, NULL );
            glCompileShader( nuanceurObj );
            glAttachShader( prog1, nuanceurObj );
            ProgNuanceur::afficherLogCompile( nuanceurObj );
            delete [] chainesTessctrl[1];
        }
        // attacher le nuanceur d'évaluation de la tessellation
        const GLchar *chainesTesseval[2] = { "#version 410\n#define NUANCEUR_TESSEVAL\n", ProgNuanceur::lireNuanceur( "nuanceurs.glsl" ) };
        if ( chainesTesseval[1] != NULL )
        {
            GLuint nuanceurObj = glCreateShader( GL_TESS_EVALUATION_SHADER );
            glShaderSource( nuanceurObj, 2, chainesTesseval, NULL );
            glCompileShader( nuanceurObj );
            glAttachShader( prog1, nuanceurObj );
            ProgNuanceur::afficherLogCompile( nuanceurObj );
            delete [] chainesTesseval[1];
        }
        // attacher le nuanceur de géometrie
        const GLchar *chainesGeometrie[2] = { "#version 410\n#define NUANCEUR_GEOMETRIE\n", ProgNuanceur::lireNuanceur( "nuanceurs.glsl" ) };
        if ( chainesGeometrie[1] != NULL )
        {
            GLuint nuanceurObj = glCreateShader( GL_GEOMETRY_SHADER );
            glShaderSource( nuanceurObj, 2, chainesGeometrie, NULL );
            glCompileShader( nuanceurObj );
            glAttachShader( prog1, nuanceurObj );
            ProgNuanceur::afficherLogCompile( nuanceurObj );
            delete [] chainesGeometrie[1];
        }
        // attacher le nuanceur de fragments
        const GLchar *chainesFragments[2] = { "#version 410\n#define NUANCEUR_FRAGMENTS\n", ProgNuanceur::lireNuanceur( "nuanceurs.glsl" ) };
        if ( chainesFragments[1] != NULL )
        {
            GLuint nuanceurObj = glCreateShader( GL_FRAGMENT_SHADER );
            glShaderSource( nuanceurObj, 2, chainesFragments, NULL );
            glCompileShader( nuanceurObj );
            glAttachShader( prog1, nuanceurObj );
            ProgNuanceur::afficherLogCompile( nuanceurObj );
            delete [] chainesFragments[1];
        }
        // faire l'édition des liens du programme
        glLinkProgram( prog1 );
        ProgNuanceur::afficherLogLink( prog1 );
        // demander la "Location" des variables
        if ( ( loc1Vertex = glGetAttribLocation( prog1, "Vertex" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de Vertex" << std::endl;
        if ( ( loc1Color = glGetAttribLocation( prog1, "Color" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de Color" << std::endl;
        if ( ( loc1matrModel = glGetUniformLocation( prog1, "matrModel" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de matrModel" << std::endl;
        if ( ( loc1matrVisu = glGetUniformLocation( prog1, "matrVisu" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de matrVisu" << std::endl;
        if ( ( loc1matrProj = glGetUniformLocation( prog1, "matrProj" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de matrProj" << std::endl;
        if ( ( loc1TessLevelInner = glGetUniformLocation( prog1, "TessLevelInner" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de TessLevelInner" << std::endl;
        if ( ( loc1TessLevelOuter = glGetUniformLocation( prog1, "TessLevelOuter" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de TessLevelOuter" << std::endl;
    }

    {
        // créer le programme
        prog2 = glCreateProgram();

        // attacher le nuanceur de sommets
        const GLchar *chainesSommets[2] = { "#version 410\n#define NUANCEUR_SOMMETS\n", ProgNuanceur::lireNuanceur( "nuanceurs.glsl" ) };
        if ( chainesSommets[1] != NULL )
        {
            GLuint nuanceurObj = glCreateShader( GL_VERTEX_SHADER );
            glShaderSource( nuanceurObj, 2, chainesSommets, NULL );
            glCompileShader( nuanceurObj );
            glAttachShader( prog2, nuanceurObj );
            ProgNuanceur::afficherLogCompile( nuanceurObj );
            delete [] chainesSommets[1];
        }
        // attacher le nuanceur de géometrie
        const GLchar *chainesGeometrie[2] = { "#version 410\n#define NUANCEUR_GEOMETRIE\n", ProgNuanceur::lireNuanceur( "nuanceurs.glsl" ) };
        if ( chainesGeometrie[1] != NULL )
        {
            GLuint nuanceurObj = glCreateShader( GL_GEOMETRY_SHADER );
            glShaderSource( nuanceurObj, 2, chainesGeometrie, NULL );
            glCompileShader( nuanceurObj );
            glAttachShader( prog2, nuanceurObj );
            ProgNuanceur::afficherLogCompile( nuanceurObj );
            delete [] chainesGeometrie[1];
        }
        // attacher le nuanceur de fragments
        const GLchar *chainesFragments[2] = { "#version 410\n#define NUANCEUR_FRAGMENTS\n", ProgNuanceur::lireNuanceur( "nuanceurs.glsl" ) };
        if ( chainesFragments[1] != NULL )
        {
            GLuint nuanceurObj = glCreateShader( GL_FRAGMENT_SHADER );
            glShaderSource( nuanceurObj, 2, chainesFragments, NULL );
            glCompileShader( nuanceurObj );
            glAttachShader( prog2, nuanceurObj );
            ProgNuanceur::afficherLogCompile( nuanceurObj );
            delete [] chainesFragments[1];
        }
        // faire l'édition des liens du programme
        glLinkProgram( prog2 );
        ProgNuanceur::afficherLogLink( prog2 );
        // demander la "Location" des variables
        if ( ( loc2Vertex = glGetAttribLocation( prog2, "Vertex" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de Vertex" << std::endl;
        if ( ( loc2Color = glGetAttribLocation( prog2, "Color" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de Color" << std::endl;
        if ( ( loc2matrModel = glGetUniformLocation( prog2, "matrModel" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de matrModel" << std::endl;
        if ( ( loc2matrVisu = glGetUniformLocation( prog2, "matrVisu" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de matrVisu" << std::endl;
        if ( ( loc2matrProj = glGetUniformLocation( prog2, "matrProj" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de matrProj" << std::endl;
    }
}

void FenetreTP::initialiser()
{
    // couleur de l'arrière-plan
    glm::vec4 couleurFond( 0.1, 0.1, 0.1, 1.0 );
    glClearColor( couleurFond.r, couleurFond.g, couleurFond.b, couleurFond.a );

    glEnable( GL_LINE_SMOOTH );
    glLineWidth( 2.0 );

    // charger les nuanceurs
    chargerNuanceurs();

    FenetreTP::VerifierErreurGL("debut initialiser");

    // Charger le modèle
    GLfloat ptsCtrl[] = { 0.0,  0.0,
                          0.2,  0.4,
                          0.4,  0.6,
                          0.6,  0.4,
                          0.8,  0.1,
                          1.0,  0.0 };

    // allouer les objets OpenGL
    glGenVertexArrays( 1, vao );
    glGenBuffers( 1, vbo );

    // initialiser le VAO
    glBindVertexArray( vao[0] );
    // charger le VBO pour les points de controle
    glBindBuffer( GL_ARRAY_BUFFER, vbo[0] );
    glBufferData( GL_ARRAY_BUFFER, sizeof(ptsCtrl), ptsCtrl, GL_STATIC_DRAW );
    glVertexAttribPointer( loc1Vertex, 2, GL_FLOAT, GL_FALSE, 0, 0 );
    glEnableVertexAttribArray(loc1Vertex);
    glBindVertexArray(0);

    FenetreTP::VerifierErreurGL("fin initialiser");
}

void FenetreTP::conclure()
{
    glUseProgram( 0 );
    glDeleteVertexArrays( 1, vao );
    glDeleteBuffers( 1, vbo );
}

void afficherModele()
{
    // informer OpenGL du niveau de tessellation
    GLfloat TessLevelOuterTab[] = { Etat::TessLevelOuter, Etat::TessLevelOuter, Etat::TessLevelOuter, Etat::TessLevelOuter };
    GLfloat TessLevelInnerTab[] = { Etat::TessLevelInner, Etat::TessLevelInner, Etat::TessLevelInner, Etat::TessLevelInner };
    glPatchParameterfv( GL_PATCH_DEFAULT_OUTER_LEVEL, TessLevelOuterTab );
    glPatchParameterfv( GL_PATCH_DEFAULT_INNER_LEVEL, TessLevelInnerTab );

    // afin de mieux voir à l'écran, changer la largeur de ligne
    //{ int l = Etat::TessLevelInner+Etat::TessLevelOuter; glLineWidth( l < 3 ? 5. : l < 11 ? 4. : l < 17 ? 3. : l < 25 ? 2. : 1. ); }

    glBindVertexArray( vao[0] );         // sélectionner le VAO

    // afficher la courbe
    glUseProgram( prog1 );
    glUniform1f( loc1TessLevelInner, Etat::TessLevelInner );
    glUniform1f( loc1TessLevelOuter, Etat::TessLevelOuter );
    glVertexAttrib3f( loc1Color, 0.5, 0.5, 1.0 );
    glPatchParameteri( GL_PATCH_VERTICES, 6 );
    glDrawArrays( GL_PATCHES, 0, 6 );

    // afficher les points de contrôle
    glUseProgram( prog2 );
    glVertexAttrib3f( loc2Color, 1.0, 0.5, 0.5 );
    glDrawArrays( GL_LINE_STRIP, 0, 6 );

    glBindVertexArray( 0 );              // désélectionner le VAO
}

void FenetreTP::afficherScene()
{
    // effacer l'ecran et le tampon de profondeur
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    // mettre à jour la projection
    matrProj.Ortho( -0.1, 1.1, -0.2, 0.8, -1.0, 1.0 );
    glUseProgram( prog1 );
    glUniformMatrix4fv( loc1matrProj, 1, GL_FALSE, matrProj );
    glUseProgram( prog2 );
    glUniformMatrix4fv( loc2matrProj, 1, GL_FALSE, matrProj );

    // mettre à jour la caméra
    matrVisu.LoadIdentity();
    glUseProgram( prog1 );
    glUniformMatrix4fv( loc1matrVisu, 1, GL_FALSE, matrVisu );
    glUseProgram( prog2 );
    glUniformMatrix4fv( loc2matrVisu, 1, GL_FALSE, matrVisu );

    // initialiser les transformations de modélisation
    matrModel.LoadIdentity();
    glUseProgram( prog1 );
    glUniformMatrix4fv( loc1matrModel, 1, GL_FALSE, matrModel );
    glUseProgram( prog2 );
    glUniformMatrix4fv( loc2matrModel, 1, GL_FALSE, matrModel );

    // dessiner la scène
    afficherModele();

    // permuter tampons avant et arrière
    swap();
}

void FenetreTP::redimensionner( GLsizei w, GLsizei h )
{
    glViewport( 0, 0, w, h );
}

void FenetreTP::clavier( TP_touche touche )
{
    switch ( touche )
    {
    case TP_ECHAP:
    case TP_q: // Quitter l'application
        quit();
        break;

    case TP_v: // Recharger les nuanceurs
        chargerNuanceurs();
        std::cout << "// Recharger nuanceurs" << std::endl;
        break;

    case TP_i: // Augmenter le niveau de tessellation interne
        ++Etat::TessLevelInner;
        std::cout << " Etat::TessLevelInner=" << Etat::TessLevelInner << " Etat::TessLevelOuter=" << Etat::TessLevelOuter << std::endl;
        break;
    case TP_k: // Diminuer le niveau de tessellation interne
        if ( --Etat::TessLevelInner < 1 ) Etat::TessLevelInner = 1;
        std::cout << " Etat::TessLevelInner=" << Etat::TessLevelInner << " Etat::TessLevelOuter=" << Etat::TessLevelOuter << std::endl;
        break;

    case TP_o: // Augmenter le niveau de tessellation externe
        ++Etat::TessLevelOuter;
        std::cout << " Etat::TessLevelInner=" << Etat::TessLevelInner << " Etat::TessLevelOuter=" << Etat::TessLevelOuter << std::endl;
        break;
    case TP_l: // Diminuer le niveau de tessellation externe
        if ( --Etat::TessLevelOuter < 1 ) Etat::TessLevelOuter = 1;
        std::cout << " Etat::TessLevelInner=" << Etat::TessLevelInner << " Etat::TessLevelOuter=" << Etat::TessLevelOuter << std::endl;
        break;

    case TP_u: // Augmenter les deux niveaux de tessellation
        ++Etat::TessLevelOuter;
        Etat::TessLevelInner = Etat::TessLevelOuter;
        std::cout << " Etat::TessLevelInner=" << Etat::TessLevelInner << " Etat::TessLevelOuter=" << Etat::TessLevelOuter << std::endl;
        break;
    case TP_j: // Diminuer les deux niveaux de tessellation
        if ( --Etat::TessLevelOuter < 1 ) Etat::TessLevelOuter = 1;
        Etat::TessLevelInner = Etat::TessLevelOuter;
        std::cout << " Etat::TessLevelInner=" << Etat::TessLevelInner << " Etat::TessLevelOuter=" << Etat::TessLevelOuter << std::endl;
        break;

    case TP_s: // Sauvegarder une copie de la fenêtre dans un fichier
        sauvegarderFenetre( );
        break;

    default:
        std::cout << " touche inconnue : " << (char) touche << std::endl;
        imprimerFichier( "touches.txt" );
        break;
    }
}

void FenetreTP::sourisClic( int button, int state, int x, int y )
{
}

void FenetreTP::sourisMolette( int x, int y )
{
}

void FenetreTP::sourisMouvement( int x, int y )
{
}

int main( int argc, char *argv[] )
{
    // créer une fenêtre
    FenetreTP fenetre( "tess" );

    // allouer des ressources et définir le contexte OpenGL
    fenetre.initialiser();

    bool boucler = true;
    while ( boucler )
    {
        // affichage
        fenetre.afficherScene();

        // récupérer les événements et appeler la fonction de rappel
        boucler = fenetre.gererEvenement();
    }

    // détruire les ressources OpenGL allouées
    fenetre.conclure();

    return 0;
}
