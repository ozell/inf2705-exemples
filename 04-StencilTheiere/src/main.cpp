#include <stdlib.h>
#include <iostream>
#include "inf2705-matrice.h"
#include "inf2705-nuanceur.h"
#include "inf2705-fenetre.h"
#include "inf2705-forme.h"
#include "Etat.h"
#include "Pipeline.h"
#include "Camera.h"

// les formes
FormeTheiere *theiere = NULL;
FormeQuad *quad = NULL;

void chargerNuanceurs()
{
    // créer le programme
    prog = glCreateProgram();

    // attacher le nuanceur de sommets
    {
        GLuint nuanceurObj = glCreateShader( GL_VERTEX_SHADER );
        glShaderSource( nuanceurObj, 1, &ProgNuanceur::chainesSommetsBase, NULL );
        glCompileShader( nuanceurObj );
        glAttachShader( prog, nuanceurObj );
        ProgNuanceur::afficherLogCompile( nuanceurObj );
    }
    // attacher le nuanceur de fragments
    {
        GLuint nuanceurObj = glCreateShader( GL_FRAGMENT_SHADER );
        glShaderSource( nuanceurObj, 1, &ProgNuanceur::chainesFragmentsBase, NULL );
        glCompileShader( nuanceurObj );
        glAttachShader( prog, nuanceurObj );
        ProgNuanceur::afficherLogCompile( nuanceurObj );
    }
    // faire l'édition des liens du programme
    glLinkProgram( prog );
    ProgNuanceur::afficherLogLink( prog );

    // demander la "Location" des variables
    if ( ( locVertex = glGetAttribLocation( prog, "Vertex" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de Vertex" << std::endl;
    if ( ( locColor = glGetAttribLocation( prog, "Color" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de Color" << std::endl;
    if ( ( locmatrModel = glGetUniformLocation( prog, "matrModel" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de matrModel" << std::endl;
    if ( ( locmatrVisu = glGetUniformLocation( prog, "matrVisu" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de matrVisu" << std::endl;
    if ( ( locmatrProj = glGetUniformLocation( prog, "matrProj" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de matrProj" << std::endl;
}

void FenetreTP::initialiser()
{
    glEnable( GL_DEPTH_TEST );
    glLineWidth( 3.0 );

    chargerNuanceurs();

    // créer quelques autres formes
    glUseProgram( prog );
    theiere = new FormeTheiere( );
    quad = new FormeQuad( );
}
void FenetreTP::conclure()
{
    delete theiere;
    delete quad;
}

void tracerRectangle()
{
    matrModel.PushMatrix();{
        matrModel.Translate( Etat::pos, -3.0, 0.0 );
        matrModel.Scale( 1.0, 6.0, 1.0 );
        glUniformMatrix4fv( locmatrModel, 1, GL_FALSE, matrModel );
        quad->afficher();
    }matrModel.PopMatrix();
}

void FenetreTP::afficherScene( )
{
    // effacer l'ecran et le tampon de profondeur et le stencil
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT );
    glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );

    glUseProgram( prog );

    // définir le pipeline graphique
    matrProj.Perspective( 30.0, (GLdouble) largeur_ / (GLdouble) hauteur_, 0.1, 30.0 );
    glUniformMatrix4fv( locmatrProj, 1, GL_FALSE, matrProj );

    matrVisu.LookAt( camera.dist*sin(camera.phi)*sin(camera.theta),
                     camera.dist*cos(camera.phi),
                     camera.dist*sin(camera.phi)*cos(camera.theta),
                     0, 0, 0,
                     0, 1, 0 );
    glUniformMatrix4fv( locmatrVisu, 1, GL_FALSE, matrVisu );

    matrModel.LoadIdentity();
    glUniformMatrix4fv( locmatrModel, 1, GL_FALSE, matrModel );

    // dessiner la scène
    if ( Etat::contenu < 1 ) goto fin;

    // // void glStencilFunc( GLenum func, GLint ref, GLuint mask );
    // // void glStencilOp( GLenum sfail, GLenum zfail, GLenum pass );

    // rectangle
    glEnable( GL_STENCIL_TEST );
    glStencilFunc( GL_ALWAYS, 1, 1 );
    glStencilOp( GL_KEEP, GL_KEEP, GL_REPLACE );
    glDisable( GL_DEPTH_TEST );

    // Explications des quelques énoncés ci-dessus:

    //  Le "GL_ALWAYS" dans l'appel à glStencilFunc() indique que le test de
    //  stencil réussit toujours.

    //  D'autre part, le premier argument de glStencilOp() est "GL_KEEP" qui
    //  indique que si le test de stencil échoue, alors la valeur dans le
    //  tampon de stencil sera "conservée" et ne sera pas modifiée.  Dans le
    //  cas présent, on a dit que le test de stencil réussit toujours
    //  ("GL_ALWAYS" de glStencilFunc()), alors ce premier argument de
    //  glStencilOp() n'est donc pas important.

    //  Le second argument de glStencilOp() est "GL_KEEP" qui indique que si
    //  le test de *profondeur* échoue, alors la valeur dans le tampon de
    //  stencil sera "conservée" et ne sera pas modifiée.  Le test de
    //  profondeur étant désactivé avec glDisable( GL_DEPTH_TEST), le test de
    //  profondeur n'a donc alors pas d'importance pour glStencilOp().

    //  Enfin, le troisième argument de glStencilOp() est "GL_REPLACE" qui
    //  indique que lorsque le test de stencil réussit ET que le test de
    //  profondeur réussit aussi, alors la valeur dans le tampon de stencil
    //  sera "remplacée" par la valeur de référence ('1') qui est donnée en
    //  second argument de glStencilFunc().

    glVertexAttrib3f( locColor, 0.0, 0.0, 1.0 );
    tracerRectangle( );
    glEnable( GL_DEPTH_TEST );

    // Indiquer de plus modifier les valeurs dans le tampon de stencil
    glStencilOp( GL_KEEP, GL_KEEP, GL_KEEP );

    // descendre un peu la théière
    matrModel.Translate( 0, -2, 0 );
    glUniformMatrix4fv( locmatrModel, 1, GL_FALSE, matrModel );
    if ( Etat::contenu < 2 ) goto fin;

    // théière fil de fer seulement lorsque le stencil contient des '1'
    glStencilFunc( GL_EQUAL, 1, 1 );
    glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
    glVertexAttrib3f( locColor, 0.0, 1.0, 0.0 );
    theiere->afficher( );
    if ( Etat::contenu < 3 ) goto fin;

    // théière pleine rouge seulement lorsque le stencil contient des '0'
    glStencilFunc( GL_EQUAL, 0, 1 );
    glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
    glVertexAttrib3f( locColor, 1.0, 0.0, 0.0 );
    theiere->afficher( );

 fin:
    glDisable( GL_STENCIL_TEST );

    // permuter tampons avant et arrière
    swap();
}

void FenetreTP::redimensionner( GLsizei w, GLsizei h )
{
    glViewport( 0, 0, w, h );
}

void FenetreTP::clavier( TP_touche touche )
{
    switch ( touche )
    {
    case TP_ECHAP:
    case TP_q: // Quitter l'application
        quit();
        break;

    case TP_v: // Recharger les nuanceurs
        chargerNuanceurs();
        std::cout << "// Recharger nuanceurs" << std::endl;
        break;

    case TP_PLUS: // Afficher plus de contenu
    case TP_EGAL:
        Etat::contenu++; if ( Etat::contenu > 3 ) Etat::contenu = 3;
        std::cout << " Etat::contenu=" << Etat::contenu << std::endl;
        break;

    case TP_SOULIGNE:
    case TP_MOINS: // Afficher moins de contenu
        Etat::contenu--; if ( Etat::contenu < 0 ) Etat::contenu = 0;
        std::cout << " Etat::contenu=" << Etat::contenu << std::endl;
        break;

    case TP_DROITE: // Déplacer le plan vers la droite
    case TP_CROCHETDROIT:
    case TP_SUPERIEUR:
        Etat::pos += 0.1;
        std::cout << " Etat::pos=" << Etat::pos << std::endl;
        break;
    case TP_GAUCHE: // Déplacer le plan vers la gauche
    case TP_CROCHETGAUCHE:
    case TP_INFERIEUR:
        Etat::pos -= 0.1;
        std::cout << " Etat::pos=" << Etat::pos << std::endl;
        break;

    case TP_s: // Sauvegarder une copie de la fenêtre dans un fichier
        sauvegarderFenetre( );
        break;

    default:
        //std::cout << " touche inconnue : " << (char) touche << std::endl;
        break;
    }
}

glm::ivec2 sourisPosPrec(0,0);
static bool presse = false;
void FenetreTP::sourisClic( int button, int state, int x, int y )
{
    presse = ( state == TP_PRESSE );
    if ( presse )
        if ( button == TP_BOUTON_GAUCHE )
        {
            sourisPosPrec.x = x;
            sourisPosPrec.y = y;
        }
}

void FenetreTP::sourisMolette( int x, int y )
{
    const int sens = +1;
    Etat::pos += float( 0.02 * sens * y );
    std::cout << " Etat::pos=" << Etat::pos << std::endl;
}

void FenetreTP::sourisMouvement( int x, int y )
{
    if ( presse )
    {
        int dx = x - sourisPosPrec.x;
        int dy = y - sourisPosPrec.y;
        camera.theta -= dx / 100.0;
        camera.phi   -= dy / 50.0;

        sourisPosPrec.x = x;
        sourisPosPrec.y = y;
    }
}

int main( int argc, char *argv[] )
{
    // créer une fenêtre TP
    FenetreTP fenetre( "stencil Theiere" );

    // allouer des ressources et définir le contexte OpenGL
    fenetre.initialiser();

    bool boucler = true;
    while ( boucler )
    {
        // affichage
        fenetre.afficherScene();

        // récupérer les événements et appeler la fonction de rappel
        boucler = fenetre.gererEvenement();
    }

    // détruire les ressources OpenGL allouées
    fenetre.conclure();

    return 0;
}
