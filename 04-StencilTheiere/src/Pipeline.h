#ifndef __PIPELINE_H__
#define __PIPELINE_H__

// variables pour l'utilisation des nuanceurs
GLuint prog;      // votre programme de nuanceurs
GLint locVertex;
GLint locColor;
GLint locmatrModel;
GLint locmatrVisu;
GLint locmatrProj;

// matrices du pipeline graphique
MatricePipeline matrModel, matrVisu, matrProj;

#endif
