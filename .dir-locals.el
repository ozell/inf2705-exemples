;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

(
 ;; (nil . ((indent-tabs-mode . t)
 ;;         (fill-column . 80)))
 (c-mode . ((c-file-style . "BSD")
            ;;(subdirs . nil)
	    ))
 (c++-mode . ((c-file-style . "inf2705")
            ;;(subdirs . nil)
	    ))
 ;; ("src/imported"
 ;;  . ((nil . ((change-log-default-name
 ;;              . "ChangeLog.local")))))
 )

;; https://www.gnu.org/software/emacs/manual/html_node/emacs/Directory-Variables.html
