#!/bin/bash

cd $1;

git mv nuanceurs*.glsl nuanceurs.glsl
git mv nuanceurs*.glsl.php nuanceurs.glsl.php
sed -i 's/nuanceurs.*.glsl/nuanceurs.glsl/' [a-z]*.cpp

git mv [a-z]*.cpp main.cpp
git mv [a-z]*.cpp.php main.cpp.php

git rm makefile --force
ln -s ../base.mk makefile
git add makefile

make run
make clean

cd -
