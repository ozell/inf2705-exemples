#include "Etat.h"

SINGLETON_DECLARATION_CPP(Etat);

bool        Etat::affichePoints = true;
float       Etat::pointsize = 10;
int         Etat::texnumero = 0;
GLuint      Etat::textureLUTIN = 0;
GLuint      Etat::textureECHIQUIER = 0;
