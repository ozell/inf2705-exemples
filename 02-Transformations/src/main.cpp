#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <math.h>
#include "inf2705-matrice.h"
#include "inf2705-nuanceur.h"
#include "inf2705-fenetre.h"
#include "inf2705-forme.h"
#include "Etat.h"
#include "Pipeline.h"
#include "Camera.h"

FormeCube *cube = NULL;
FormeTri *tri = NULL;
FormeQuad *quad = NULL;
FormeSphere *sphere = NULL;
FormeCylindre *cylindre = NULL;

std::string fichiers[9] = {
    "transformations1.txt",
    "transformations2.txt",
    "transformations3.txt",
    "transformations4.txt",
    "transformations5.txt",
    "transformations6.txt",
    "transformations7.txt",
    "transformations8.txt",
    "transformations9.txt"
};
std::string fichierCourant = fichiers[0];

void calculerPhysique( )
{
    // ajuster le dt selon la fréquence d'affichage
    {
        static int tempsPrec = 0;
        // obtenir le temps depuis l'initialisation (en millisecondes)
        int tempsCour = FenetreTP::obtenirTemps();
        // calculer un nouveau dt (sauf la première fois)
        if ( tempsPrec ) Etat::dt = float(( tempsCour - tempsPrec )/1000.0);
        // se préparer pour la prochaine fois
        tempsPrec = tempsCour;
    }

    if ( Etat::enmouvement )
    {
        static int sensTheta = 1;
        static int sensPhi = 1;
        camera.theta += 15.0 * Etat::dt * sensTheta;
        camera.phi += 25.0 * Etat::dt * sensPhi;
        if ( camera.theta <= -40. || camera.theta >= 40.0 ) sensTheta = -sensTheta;
        if ( camera.phi < -40. || camera.phi > 40. ) sensPhi = -sensPhi;
    }

    camera.verifierAngles();
}

void chargerNuanceurs()
{
    // charger le nuanceur de base
    {
        // créer le programme
        progBase = glCreateProgram();

        // attacher le nuanceur de sommets
        {
            GLuint nuanceurObj = glCreateShader( GL_VERTEX_SHADER );
            glShaderSource( nuanceurObj, 1, &ProgNuanceur::chainesSommetsBase, NULL );
            glCompileShader( nuanceurObj );
            glAttachShader( progBase, nuanceurObj );
            ProgNuanceur::afficherLogCompile( nuanceurObj );
        }
        // attacher le nuanceur de fragments
        {
            GLuint nuanceurObj = glCreateShader( GL_FRAGMENT_SHADER );
            glShaderSource( nuanceurObj, 1, &ProgNuanceur::chainesFragmentsBase, NULL );
            glCompileShader( nuanceurObj );
            glAttachShader( progBase, nuanceurObj );
            ProgNuanceur::afficherLogCompile( nuanceurObj );
        }

        // faire l'édition des liens du programme
        glLinkProgram( progBase );
        ProgNuanceur::afficherLogLink( progBase );

        // demander la "Location" des variables
        if ( ( locColor = glGetAttribLocation( progBase, "Color" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de Color" << std::endl;
        if ( ( locmatrModel = glGetUniformLocation( progBase, "matrModel" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de matrModel" << std::endl;
        if ( ( locmatrVisu = glGetUniformLocation( progBase, "matrVisu" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de matrVisu" << std::endl;
        if ( ( locmatrProj = glGetUniformLocation( progBase, "matrProj" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de matrProj" << std::endl;
    }
}

void FenetreTP::initialiser()
{
    // couleur de l'arrière-plan
    glm::vec4 couleurFond( 0.2, 0.2, 0.2, 1.0 );
    glClearColor( couleurFond.r, couleurFond.g, couleurFond.b, couleurFond.a );

    glEnable( GL_DEPTH_TEST );
    glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );

    // charger les nuanceurs
    chargerNuanceurs();
    glUseProgram( progBase );

    // créer quelques autres formes
    cube = new FormeCube( 1.0, true );
    quad = new FormeQuad( 1.0, true );
    tri = new FormeTri( 1.0, true );
    sphere = new FormeSphere( 1.0, 20, 20, true );
    cylindre = new FormeCylindre( 1.0, 1.0, 1.0, 20, 1, true );

    FenetreTP::VerifierErreurGL("fin initialiser");
}

void FenetreTP::conclure()
{
    glUseProgram( 0 );
    delete cube;
    delete quad;
    delete tri;
    delete sphere;
    delete cylindre;
}

void afficherModele()
{
    // afficher le modèle
    matrModel.LoadIdentity();
    glUniformMatrix4fv( locmatrModel, 1, GL_FALSE, matrModel );
    bool matrModelModifiee = true; // la matrModel est modifiée et doit être envoyée au GPU par un appel à glUniformMatrix4fv()

    // lire et appliquer les transformations
    std::ifstream transformations( fichierCourant );
    std::string ligne; int iligne=0;
    // pour chaque ligne lue ...
    while ( std::getline( transformations, ligne ) )
    {
        std::stringstream lalignecourante(ligne); std::string tok; lalignecourante >> tok;
        if ( Etat::echoTransformations && Etat::nlignes > 0 ) std::cout << std::setw(3) << iligne+1 << " : ";
        switch ( tok[0] )
        {
            float angle, x, y, z;

        case 'I': // Identity
            if ( Etat::echoTransformations ) std::cout << "Identity( );" << std::endl;
            matrModel.LoadIdentity( );
            matrModelModifiee = true;
            break;
        case 'R': // Rotation angle vx vy vz
            angle = x = y = z = 0; lalignecourante >> angle >> x >> y >> z;
            if ( Etat::echoTransformations ) std::cout << "Rotate( " << angle << ", " << x << ", " << y << ", " << z << " )" << std::endl;
            matrModel.Rotate( angle, x, y, z );
            matrModelModifiee = true;
            break;
        case 'T': // Translate tx ty tz
            x = y = z = 0; lalignecourante >> x >> y >> z;
            if ( Etat::echoTransformations ) std::cout << "Translate( " << x << ", " << y << ", " << z << " )" << std::endl;
            matrModel.Translate( x, y, z );
            matrModelModifiee = true;
            break;
        case 'S': // Scale sx sy sz
            x = y = z = 1; lalignecourante >> x >> y >> z;
            if ( Etat::echoTransformations ) std::cout << "Scale( " << x << ", " << y << ", " << z << " )" << std::endl;
            matrModel.Scale( x, y, z );
            matrModelModifiee = true;
            break;
        case '{': // PushMatrix
        case '[': // PushMatrix
            if ( Etat::echoTransformations ) std::cout << "PushMatrix( );" << std::endl;
            matrModel.PushMatrix( );
            matrModelModifiee = true;
            break;
        case '}': // PopMatrix
        case ']': // PopMatrix
            if ( Etat::echoTransformations ) std::cout << "PopMatrix( );" << std::endl;
            matrModel.PopMatrix( );
            matrModelModifiee = true;
            break;

        case 'C': // Couleur r g b
            x = y = z = 1; lalignecourante >> x >> y >> z;
            if ( Etat::echoTransformations ) std::cout << "Couleur( " << x << ", " << y << ", " << z << " )" << std::endl;
            glVertexAttrib3f( locColor, x, y, z );
            break;

        case 't': // triangle
            if ( Etat::echoTransformations ) std::cout << "triangle;" << std::endl;
            if ( matrModelModifiee ) { glUniformMatrix4fv( locmatrModel, 1, GL_FALSE, matrModel ); } matrModelModifiee = false;
            tri->afficher();
            break;
        case 'q': // quadrilatère
            if ( Etat::echoTransformations ) std::cout << "quadrilatère;" << std::endl;
            if ( matrModelModifiee ) { glUniformMatrix4fv( locmatrModel, 1, GL_FALSE, matrModel ); } matrModelModifiee = false;
            quad->afficher();
            break;
        case 'c': // cube
        case 'u': // cube
            if ( Etat::echoTransformations ) std::cout << "cube;" << std::endl;
            if ( matrModelModifiee ) { glUniformMatrix4fv( locmatrModel, 1, GL_FALSE, matrModel ); } matrModelModifiee = false;
            cube->afficher();
            break;
        case 'l': // cylindre
        case 'y': // cylindre
            if ( Etat::echoTransformations ) std::cout << "cylindre;" << std::endl;
            if ( matrModelModifiee ) { glUniformMatrix4fv( locmatrModel, 1, GL_FALSE, matrModel ); } matrModelModifiee = false;
            cylindre->afficher();
            break;
        case 's': // sphère
            if ( Etat::echoTransformations ) std::cout << "sphere;" << std::endl;
            if ( matrModelModifiee ) { glUniformMatrix4fv( locmatrModel, 1, GL_FALSE, matrModel ); } matrModelModifiee = false;
            sphere->afficher();
            break;

        default:
            if ( Etat::echoTransformations ) std::cout << std::endl;
            break;
        }
        // afficher les axes
        if ( Etat::afficheAxes )
            if ( ( Etat::nlignes == 0 ) || ( Etat::nlignes == ++iligne ) )
                FenetreTP::afficherAxes( 1.0 );

        // le nombre max de lignes est atteint ?
        if ( Etat::nlignes > 0 && Etat::nlignes <= iligne ) break;
    }
    transformations.close();

    if ( Etat::echoTransformations ) std::cout << std::endl;
    Etat::echoTransformations = false;

    FenetreTP::VerifierErreurGL("fin afficherScene");
}

void FenetreTP::afficherScene()
{
    // effacer l'ecran et le tampon de profondeur
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    glUseProgram( progBase );

    // mettre à jour la projection
    GLdouble aspect = (GLdouble) largeur_ / (GLdouble) hauteur_;
    if ( Etat::enPerspective )
        matrProj.Perspective( 30.0, aspect, 1.0, 100.0 );
    else
        matrProj.Ortho( -3.0, 3.0, -3.0, 3.0, 1.0, 100.0 );
    glUniformMatrix4fv( locmatrProj, 1, GL_FALSE, matrProj );

    // mettre à jour la caméra
    camera.definir();
    glUniformMatrix4fv( locmatrVisu, 1, GL_FALSE, matrVisu );

    // afficher les axes
    if ( Etat::afficheAxes ) FenetreTP::afficherAxes( 1.0 );

    // dessiner la scène
    glVertexAttrib3f( locColor, 1.0, 1.0, 1.0 );
    afficherModele();

    FenetreTP::VerifierErreurGL("fin afficherScene");

    // permuter tampons avant et arrière
    swap();
}

void FenetreTP::redimensionner( GLsizei w, GLsizei h )
{
    glViewport( 0, 0, w, h );
}

void FenetreTP::clavier( TP_touche touche )
{
    switch ( touche )
    {
    case TP_ECHAP:
    case TP_q: // Quitter l'application
        quit();
        break;

    case TP_x: // Activer/désactiver l'affichage des axes
        Etat::afficheAxes = !Etat::afficheAxes;
        std::cout << "// Affichage des axes ? " << ( Etat::afficheAxes ? "OUI" : "NON" ) << std::endl;
        break;

    case TP_v: // Recharger les nuanceurs
        chargerNuanceurs();
        std::cout << "// Recharger nuanceurs" << std::endl;
        break;

    case TP_g: // Permuter l'affichage en fil de fer ou plein
        {
            GLint modePlein[2];
            glGetIntegerv( GL_POLYGON_MODE, modePlein );
            glPolygonMode( GL_FRONT_AND_BACK, ( modePlein[0] == GL_LINE ) ? GL_FILL : GL_LINE );
        }
        break;

    case TP_c: // Permuter le "culling" des faces arrières
        {
            GLint modeCulling;
            glGetIntegerv( GL_CULL_FACE, &modeCulling );
            if ( modeCulling == GL_TRUE )
                glDisable( GL_CULL_FACE );
            else
                glEnable( GL_CULL_FACE );
        }
        break;

    case TP_p: // Permuter la projection: perspective ou orthogonale
        Etat::enPerspective = !Etat::enPerspective;
        break;

    case TP_SOULIGNE:
    case TP_MOINS: // Incrémenter la distance de la caméra
        camera.dist += 0.5;
        break;

    case TP_PLUS: // Décrémenter la distance de la caméra
    case TP_EGAL:
        camera.dist -= 0.5;
        break;

    case TP_GAUCHE: // Augmenter theta
        camera.theta += 0.5;
        break;

    case TP_DROITE: // Décrémenter theta
        camera.theta -= 0.5;
        break;

    case TP_HAUT: // Augmenter phi
        camera.phi += 0.5;
        break;

    case TP_BAS:  // Décrémenter phi
        camera.phi -= 0.5;
        break;

    case TP_i: // Remettre les angles de la caméra à 0
        camera.theta = camera.phi = 0;
        break;

    case TP_r: // Réinitiliaser la caméra (angles et distance)
        camera.theta = camera.phi = 0.0;
        camera.dist = 10;
        break;

    case TP_1: // Afficher le fichier transformations1
    case TP_2: // Afficher le fichier transformations2
    case TP_3: // Afficher le fichier transformations3
    case TP_4: // Afficher le fichier transformations4
    case TP_5: // Afficher le fichier transformations5
    case TP_6: // Afficher le fichier transformations6
    case TP_7: // Afficher le fichier transformations7
    case TP_8: // Afficher le fichier transformations8
    case TP_9: // Afficher le fichier transformations9
        fichierCourant = fichiers[touche-TP_1];
        std::cout << " fichierCourant=" << fichierCourant << std::endl;
        break;

    case TP_CROCHETGAUCHE: // Décrémenter le nombre de lignes utilisées du fichier d'entrée
    case TP_PAGEPREC: // Décrémenter le nombre de lignes utilisées du fichier d'entrée
        if ( Etat::nlignes > 0 ) Etat::nlignes--;
        if ( Etat::nlignes > 0 )
        {
            Etat::echoTransformations = true;
            std::cout << "### NOMBRE DE LIGNES AFFICHÉES ### nlignes=" << Etat::nlignes << std::endl;
        }
        break;

    case TP_CROCHETDROIT: // Incrémenter le nombre de lignes utilisées du fichier d'entrée
    case TP_PAGESUIV: // Incrémenter le nombre de lignes utilisées du fichier d'entrée
        Etat::echoTransformations = true;
        Etat::nlignes++;
        std::cout << "### NOMBRE DE LIGNES AFFICHÉES ### nlignes=" << Etat::nlignes << std::endl;
        break;

    case TP_ESPACE: // Permuter la rotation automatique du modèle
        Etat::enmouvement = !Etat::enmouvement;
        break;

    case TP_s: // Sauvegarder une copie de la fenêtre dans un fichier
        sauvegarderFenetre( );
        break;

    default:
        std::cout << " touche inconnue : " << (char) touche << std::endl;
        imprimerFichier( "touches.txt" );
        imprimerFichier( "syntaxe.txt" );
        break;
    }
}

glm::ivec2 sourisPosPrec(0,0);
static bool presse = false;
void FenetreTP::sourisClic( int button, int state, int x, int y )
{
    presse = ( state == TP_PRESSE );
    if ( presse )
        if ( button == TP_BOUTON_GAUCHE )
        {
            sourisPosPrec.x = x;
            sourisPosPrec.y = y;
        }
}

void FenetreTP::sourisMolette( int x, int y )
{
}

void FenetreTP::sourisMouvement( int x, int y )
{
    if ( presse )
    {
        int dx = x - sourisPosPrec.x;
        int dy = y - sourisPosPrec.y;
        camera.theta -= dx / 3.0;
        camera.phi   += dy / 3.0;

        sourisPosPrec.x = x;
        sourisPosPrec.y = y;
        camera.verifierAngles();
    }
}

int main( int argc, char *argv[] )
{
    // créer une fenêtre
    FenetreTP fenetre( "02-transformation", 700, 700 );

    // allouer des ressources et définir le contexte OpenGL
    fenetre.initialiser();

    bool boucler = true;
    while ( boucler )
    {
        // mettre à jour la physique
        calculerPhysique( );

        // affichage
        fenetre.afficherScene();

        // récupérer les événements et appeler la fonction de rappel
        boucler = fenetre.gererEvenement();
    }

    // détruire les ressources OpenGL allouées
    fenetre.conclure();

    return 0;
}
