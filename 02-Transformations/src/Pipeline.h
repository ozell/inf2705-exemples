#ifndef __PIPELINE_H__
#define __PIPELINE_H__

// variables pour l'utilisation des nuanceurs
GLuint progBase;
GLint locVertex;
GLint locColor;
GLint locmatrModel;
GLint locmatrVisu;
GLint locmatrProj;

// matrices du pipeline graphique
MatricePipeline matrModel, matrVisu, matrProj;

#endif
